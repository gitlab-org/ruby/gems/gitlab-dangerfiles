# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Dangerfiles::Spin do
  include_context "with teammates"

  describe "#==" do
    it "compares Spin attributes" do
      spin1 = described_class.new(:backend, frontend_reviewer, frontend_maintainer, false)
      spin2 = described_class.new(:backend, frontend_reviewer, frontend_maintainer, false)
      spin3 = described_class.new(:backend, frontend_reviewer, frontend_maintainer, true)
      spin4 = described_class.new(:backend, frontend_reviewer, backend_maintainer, false)
      spin5 = described_class.new(:backend, backend_maintainer, frontend_maintainer, false)
      spin6 = described_class.new(:frontend, frontend_reviewer, frontend_maintainer, false)

      expect(spin1).to eq(spin2)
      expect(spin1).not_to eq(spin3)
      expect(spin1).not_to eq(spin4)
      expect(spin1).not_to eq(spin5)
      expect(spin1).not_to eq(spin6)
    end
  end
end
