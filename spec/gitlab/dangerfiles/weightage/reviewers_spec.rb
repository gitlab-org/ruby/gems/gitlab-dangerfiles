# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/weightage/reviewers"

RSpec.describe Gitlab::Dangerfiles::Weightage::Reviewers do
  let(:multiplier) { Gitlab::Dangerfiles::Weightage::CAPACITY_MULTIPLIER }
  let(:regular_reviewer) { double("Teammate", hungry: false, reduced_capacity: false, only_maintainer_reviews: false) }
  let(:hungry_reviewer) { double("Teammate", hungry: true, reduced_capacity: false, only_maintainer_reviews: false) }
  let(:reduced_capacity_reviewer) { double("Teammate", hungry: false, reduced_capacity: true, only_maintainer_reviews: false) }
  let(:reviewers) do
    [
      hungry_reviewer,
      regular_reviewer,
      reduced_capacity_reviewer,
    ]
  end

  let(:regular_traintainer) { double("Teammate", hungry: false, reduced_capacity: false, only_maintainer_reviews: false) }
  let(:hungry_traintainer) { double("Teammate", hungry: true, reduced_capacity: false, only_maintainer_reviews: false) }
  let(:reduced_capacity_traintainer) { double("Teammate", hungry: false, reduced_capacity: true, only_maintainer_reviews: false) }
  let(:traintainers) do
    [
      hungry_traintainer,
      regular_traintainer,
      reduced_capacity_traintainer,
    ]
  end

  let(:hungry_reviewer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT * multiplier + described_class::DEFAULT_REVIEWER_WEIGHT }
  let(:hungry_traintainer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT * multiplier + described_class::DEFAULT_REVIEWER_WEIGHT + described_class::TRAINTAINER_WEIGHT }
  let(:reviewer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT * multiplier }
  let(:traintainer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT * multiplier }
  let(:reduced_capacity_reviewer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT }
  let(:reduced_capacity_traintainer_count) { Gitlab::Dangerfiles::Weightage::BASE_REVIEWER_WEIGHT }
  let(:weighted_reviewers_result) do
    hungry_reviewer_count + reviewer_count + reduced_capacity_reviewer_count +
      hungry_traintainer_count + traintainer_count + reduced_capacity_traintainer_count
  end

  subject(:weighted_reviewers) { described_class.new(reviewers, traintainers).execute }

  describe "#execute", :aggregate_failures do
    it "weights the reviewers overall" do
      expect(weighted_reviewers.count).to eq weighted_reviewers_result
    end

    it "has total count of hungry reviewers and traintainers" do
      expect(weighted_reviewers.count(&:hungry)).to eq hungry_reviewer_count + hungry_traintainer_count
      expect(weighted_reviewers.count { |r| r.object_id == hungry_reviewer.object_id }).to eq hungry_reviewer_count
      expect(weighted_reviewers.count { |r| r.object_id == hungry_traintainer.object_id }).to eq hungry_traintainer_count
    end

    it "has total count of regular reviewers and traintainers" do
      expect(weighted_reviewers.count { |r| r.object_id == regular_reviewer.object_id }).to eq reviewer_count
      expect(weighted_reviewers.count { |r| r.object_id == regular_traintainer.object_id }).to eq traintainer_count
    end

    it "has count of reduced capacity reviewers" do
      expect(weighted_reviewers.count(&:reduced_capacity)).to eq reduced_capacity_reviewer_count + reduced_capacity_traintainer_count
      expect(weighted_reviewers.count { |r| r.object_id == reduced_capacity_reviewer.object_id }).to eq reduced_capacity_reviewer_count
      expect(weighted_reviewers.count { |r| r.object_id == reduced_capacity_traintainer.object_id }).to eq reduced_capacity_traintainer_count
    end

    context "when a traintainer is also a reviewer" do
      let(:reviewers) do
        [
          hungry_reviewer,
          regular_reviewer,
          reduced_capacity_reviewer,
          regular_traintainer,
        ]
      end

      it "removes the reviewer entry for a traintainer" do
        expect(weighted_reviewers.count).to eq weighted_reviewers_result
      end
    end

    context "when a reviewer has signalled they want maintainer only reviews" do
      let(:maintainer_only_reviewer) { double("Teammate", hungry: false, reduced_capacity: false, only_maintainer_reviews: true) }

      let(:reviewers) do
        [
          hungry_reviewer,
          regular_reviewer,
          reduced_capacity_reviewer,
          maintainer_only_reviewer,
        ]
      end

      it "removes the reviewer entry" do
        expect(weighted_reviewers.count).to eq weighted_reviewers_result
      end
    end
  end
end
