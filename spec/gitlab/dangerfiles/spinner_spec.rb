# frozen_string_literal: true

require "spec_helper"

RSpec.describe Gitlab::Dangerfiles::Spinner do
  include_context "with teammates"

  let(:author) do
    Gitlab::Dangerfiles::Teammate.new(
      "username" => "johndoe",
      "available" => true,
      "tz_offset_hours" => 0.0
    )
  end

  subject { described_class.new(project: "gitlab", author: author.username) }

  describe "#spin_for_approver" do
    context "when there is a single approval rule" do
      let(:rule) do
        {
          "eligible_approvers" => [valid_reviewer, invalid_reviewer]
        }
      end

      let(:valid_reviewer) do
        {
          "id" => 123,
          "username" => backend_maintainer.username,
          "name" => backend_maintainer.name,
          "state" => "active"
        }
      end

      let(:invalid_reviewer) do
        {
          "id" => 321,
          "username" => "not-reviewer",
          "name" => "Not Reviewer",
          "state" => "active"
        }
      end

      it "returns the required approval with a valid reviewer" do
        approver = subject.spin_for_approver(rule)

        expect(approver.username).to eq(backend_maintainer.username)
      end

      context "when there are no valid reviewers available" do
        let(:backend_available) { false }

        it "returns the required approval with a fallback reviewer" do
          approver = subject.spin_for_approver(rule)

          expect(approver.username).to eq(valid_reviewer["username"]).or(eq(invalid_reviewer["username"]))
        end
      end

      context "when no one is in the right project" do
        let(:backend_maintainer_project) { {} }

        it "returns the required approval with a fallback reviewer" do
          approver = subject.spin_for_approver(rule)

          expect(approver.username).to eq(valid_reviewer["username"]).or(eq(invalid_reviewer["username"]))
        end
      end
    end
  end

  describe "#spin_for_person" do
    let(:person_tz_offset_hours) { 0.0 }
    let(:joe) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "joe",
        "available" => true,
        "tz_offset_hours" => person_tz_offset_hours
      )
    end

    let(:jane) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "jane",
        "available" => true,
        "tz_offset_hours" => person_tz_offset_hours
      )
    end

    let(:unavailable) do
      Gitlab::Dangerfiles::Teammate.new(
        "username" => "janedoe",
        "available" => false,
        "tz_offset_hours" => 0.0
      )
    end

    (-4..4).each do |utc_offset|
      context "when local hour for person is #{10 + utc_offset} (offset: #{utc_offset})" do
        let(:person_tz_offset_hours) { utc_offset }

        it "returns a random person" do
          persons = [joe, jane]

          selected = subject.__send__(:spin_for_person, persons)

          expect(persons.map(&:username)).to include(selected.username)
        end
      end
    end

    ((-12..-5).to_a + (5..12).to_a).each do |utc_offset|
      context "when local hour for person is #{10 + utc_offset} (offset: #{utc_offset})" do
        let(:person_tz_offset_hours) { utc_offset }

        it "returns a random person or nil" do
          persons = [joe, jane]

          selected = subject.__send__(:spin_for_person, persons)

          expect(persons.map(&:username)).to include(selected.username)
        end
      end
    end

    it "excludes unavailable persons" do
      expect(subject.__send__(:spin_for_person, [unavailable])).to be_nil
    end

    it "excludes author" do
      expect(subject.__send__(:spin_for_person, [author])).to be_nil
    end
  end
end
